export default {
	"name": "button",
	"description": "Multipe point to attempt to find some of the most common problems with tables",
	"context": "page",
	"test": [
		// Search Composites
		{
			"name": "Invalid Primary Button",
			"test": (verbose, utils) => {

				const INVALID_PRIMARY_BUTTON_BY_TEXT = {
					src: false,
					type: "error",
					id: "invalid-primary-button-based-on-text",
					issue: "Should not be a primary button",
					resolve: 'Clear, Cancel, Refresh and Reset Buttons are not valid primary button types.',
					code: "0000",
					error: "Invalid Primary Button",
					script: "button"
				}

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var buttons = bodyWrapper.querySelectorAll('button');

				console.info("Checking Buttons - primary status");

				if (buttons.length) {

					var invalidPrimarybyText = ['clear', 'cancel', 'reset', 'refresh'];

					for (var b = 0, bLen = buttons.length; b < bLen; b++) {

						let button = buttons[b];
						let buttonText = button.textContent.toLowerCase().trim();

						let primaryButton = (button.classList.contains('cui-button-primary')) ? true : false;

						if (primaryButton) {

							if (invalidPrimarybyText.indexOf(buttonText) !== -1) {

								let issue = utils.objectAssign(INVALID_PRIMARY_BUTTON_BY_TEXT);

								issue.src = button;

								issues.push(issue); 
							}

						}

					}

				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		}
	]
}