export default {
	"name": "empty",
	"description": "Multipe point test to verify that rows, sections and tables appear in the correct grid context.",
	"context": "page",
	"test": [
		// Rows Test
		{
			name: "Empty rows",
			test: (verbose, utils) => {

				const EMPTY_ROW = {
					src: false,
					type: "error",
					id: "empty_row",
					issue: "Empty Row",
					resolve: "Rows should not be included as part of the page if they are empty. Please verify all conditional logic skip this row entirely when its contents can be skipped, or simply remove this row if its not needed.",
					code: "0000",
					error: "Empty Row",
					script: "empty"
				};

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var rows = bodyWrapper.querySelectorAll('.cui-row');

				console.info("Checking for empty rows");

				if (rows.length) {

					rows:
					for (var r = 0, rLen = rows.length; r < rLen; r++) {

						var children = rows[r].childNodes;

						if (children.length === 0) {

							let parents = utils.elementParents(rows[r], true);

							var skipEmpty = false;

							// Verify we are not inside of the tax payer header
							rowParentCheck:
							for (let p = 0, pLen = parents.length; p < pLen; p++) {

								let pNode = parents[p];

								if ((pNode.tagName === "FORM" && pNode.id === "form_tp_info") || (pNode.tagName === "FORM" && pNode.id === "form_search")) {

									skipEmpty = true;
									break;
								}
							}

							if (!skipEmpty) {

								let issue = utils.objectAssign(EMPTY_ROW);

								issue.src = rows[r];

								issues.push(issue);
							}

						}
						else {

							let elementChildren = 0;

							rowChildren:
							for (let rc = 0, rcLen = children.length; rc < rcLen; rc++) {

								if (children[rc].nodeType === 1) {
									elementChildren++;
								}

							}

							if (elementChildren === 0) {

								let parents = utils.elementParents(rows[r], true);

								var skipEmpty = false;

								// Verify we are not inside of the tax payer header
								rowParentCheck:
								for (let p = 0, pLen = parents.length; p < pLen; p++) {

									let pNode = parents[p];

									if ((pNode.tagName === "FORM" && pNode.id === "form_tp_info") || (pNode.tagName === "FORM" && pNode.id === "form_search")) {

										skipEmpty = true;
										break;
									}
								}

								if (!skipEmpty) {

									let issue = utils.objectAssign(EMPTY_ROW);

									issue.src = rows[r];

									issues.push(issue);
								}
							}

						}

					}
				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
		{
			name: "Empty Columns",
			test: (verbose, utils) => {

				const EMPTY_COLUMN = {
					src: false,
					type: "error",
					id: "empty_column",
					issue: "Empty Column",
					resolve: 'Columns should not be included as part of the page if they are empty.',
					code: "0001",
					error: "Empty Column",
					script: "empty"
				};

				const EMPTY_COLUMNS = {
					src: false,
					type: "error",
					id: "empty_column",
					issue: "Empty Columns",
					resolve: 'All the columns in the row have no contents. Please review conditional requirements and verify if the row and columns are needed.',
					code: "0002",
					error: "Empty Columns",
					script: "empty"
				};

				const rightRider = 'If column is in place to take up and force the next column to the right, remove the first column and use the push="half" attribute on the column that needs to appear on the right side of the page.';

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var rows = bodyWrapper.querySelectorAll('.cui-row');

				console.info("Checking for empty columns");

				if (rows.length) {

					allRows:
					for (var r = 0, rLen = rows.length; r < rLen; r++) {

						let columns = rows[r].querySelectorAll('.emp-col-half, .emp-col-full');

						if (columns.length) {

							let columnContents = [];

							columns:
							for (var c = 0, cLen = columns.length; c < cLen; c++) {

								let columnChildren = columns[c].childNodes;

								columnsChildren:
								for (var cc = 0, ccLen = columnChildren.length; cc < ccLen; cc++) {

									if (columnChildren[cc].nodeType === 1) {

										columnContents.push({
											column: columns[c],
											children:true
										});
										
										continue columns;
									}

								}

								columnContents.push({
									column: columns[c],
									children: false
								});
							}

							if (columnContents.length === 2) {

								// The left is in error But might need an extended message
								if (columnContents[0].children === false && columnContents[1].children === true) {

									let issue = utils.objectAssign(EMPTY_COLUMN);

									issue.type = "warning";

									issue.resolve += " " + rightRider;

									issue.src = columnContents[0].column;

									issues.push(issue);

								}
								// The right is in error
								else if (columnContents[0].children === true && columnContents[1].children === false) {

									let issue = utils.objectAssign(EMPTY_COLUMN);

									issue.src = columnContents[1].column;

									issues.push(issue);
								}
								// Both are in error
								else if (columnContents[0].children === false && columnContents[1].children === false) {

									let issue = utils.objectAssign(EMPTY_COLUMNS);

									issue.src = rows[r];

									issues.push(issue);
								}

							}

						}
					}
				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
	]
}