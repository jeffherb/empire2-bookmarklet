export default {
	"name": "gridLayout",
	"description": "Multipe point test to verify that rows, sections and columns appear in the correct grid context.",
	"context": "page",
	"test": [
		// Section Test
		{
			name: "Section Check",
			test: (verbose, utils) => {

				const INVALID_SECTION_ELEMENT = {
					src: false,
					type: "error",
					id: "invalid_section_element",
					issue: "Element directly inside section does not match expected grid layout",
					resolve: "All content should appear in some type of a grid column. Make sure everything is wrapped inside of E2:Column tag. Exceptions would include: Hidden Inputs",
					code: "0000",
					error: "Content not inside a grid system",
					script: "gridLayout"
				};

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var sections = bodyWrapper.querySelectorAll('section');

				console.info("Checking section contents");

				if (sections.length) {

					section:
					for (let s = 0, sLen = sections.length; s < sLen; s++) {

						let sChildren = sections[s].childNodes;

						sectionChildren:
						for (let sc = 0, scLen = sChildren.length; sc < scLen; sc++) {

							let child = sChildren[sc];

							// Filter out all but the actual DOM elements
							if (child.nodeType === 1) {

								// We ignore these elments because they are allowed directly inside of a sections
								var ignorableElems = ['SECTION', 'HEADER'];

								if (ignorableElems.indexOf(child.nodeName) === -1) {

									switch (child.nodeName) {

										case 'DIV':

											var classes = child.className;

											var validDivElements = ['cui-row', 'cui-row emp-button-row', 'emp-table', 'emp-inline-sections', 'emp-section-in-section-container'];

											if (classes === undefined || classes === null || validDivElements.indexOf(classes) === -1) {

												if (classes.indexOf('emp-field') !== -1) {

													var childInput = child.querySelectorAll('input');

													if (childInput.length) {

														var subChild = childInput[0];

														var subChildType = subChild.getAttribute('type');

														if (subChildType !== "hidden") {

															let issue = utils.objectAssign(INVALID_SECTION_ELEMENT);

															issue.src = child;

															issues.push(issue);

														}

													}
													else {

														let issue = utils.objectAssign(INVALID_SECTION_ELEMENT);

														issue.src = child;

														issues.push(issue);

													}

												}
												else {

													let issue = utils.objectAssign(INVALID_SECTION_ELEMENT);

													issue.src = child;

													issues.push(issue);
												}


											}

											break;
									}
								}
							}
						}
					}
				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
		// Row Test - Check for non standard elements and hidden inputs
		{
			name: "Row Check",
			test: (verbose, utils) => {

				const INVALID_ROW_ELEMENT = {
					src: false,
					type: "error",
					id: "unknown-row-element",
					issue: "Element found outside expected grid system markup",
					resolve: "Unknown (+) tag directly inside of a row. All content should be wrapped inside of E2:Column tag. Exceptions to this rule include Hidden Inputs, Groups and Tables. For additional information review Empire 2 Developer Documentation.",
					code: "0001",
					error: "Content not inside expected grid layout",
					script: "gridLayout"
				};

				const INVALID_FIELD_PLACEMENT = {
					src: false,
					type: "error",
					id: "row-with-hidden-inputs",
					issue: "Element found outside expected grid system markup",
					resolve: "Field found directly inside of a row. All fields should be placed inside of a E2:Column or table cell.",
					code: "0002",
					error: "Content not inside expected grid layout",
					script: "gridLayout"
				};

				const HIDDEN_INPUT_IN_ROW = {
					src: false,
					type: "error",
					id: "hidden-input-in-row",
					issue: "Invalid Hidden Input Placement",
					resolve: "Hidden fields provide no visual input and because of this, do not need to appear inside of grid markup directly. It is better to place hidden input with related fields in the same column or directly into the form they are submitted with.",
					code: "0003",
					error: "Content not inside expected grid layout",
					script: "gridLayout"
				};

				const INVALID_NUMBER_OF_CHILDREN = {
					src: false,
					type: "error",
					id: "too-many-row-children",
					issue: "Too many children discovered in row",
					resolve: "Too many children exist in the current row. There is a hard limit of 2 children (columns) per row in Empire 2. Current row has + children.",
					code: "0007",
					error: "Too many columns in row",
					script: "gridLayout"
				};

				const TOO_MANY_FULL_COLUMNS = {
					src: false,
					type: "error",
					id: "too-many-full-columns",
					issue: "Too many full length columns",
					resolve: "The current row has more than one full column inside of it. A row is only allowed to have a single full column; or 2 half columns.",
					code: "0008",
					error: "Too many full columns",
					script: "gridLayout"
				};

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var rows = bodyWrapper.querySelectorAll('.cui-row');

				console.info("Checking row contents");

				if (rows.length) {

					row:
					for (let r = 0, rLen = rows.length; r < rLen; r++) {

						// Skip button rows
						if (rows[r].classList.contains('emp-button-row')) {
							continue row;
						}

						let rChildren = rows[r].childNodes;

						let childElemCount = 0;
						let childElems = [];

						for (var rc = 0, rcLen = rChildren.length; rc < rcLen; rc++) {

							let node = rChildren[rc];

							if (node.nodeType === 1) {

								childElemCount++;

								childElems.push(node);
							}
						}

						if (childElemCount > 2) {

							let parents = utils.elementParents(rows[r], true);

							// Verify we are not inside of the tax payer header
							rowParentCheck:
							for (let p = 0, pLen = parents.length; p < pLen; p++) {

								let pNode = parents[p];

								if ((pNode.tagName === "FORM" && pNode.id === "form_tp_info") || (pNode.tagName === "FORM" && pNode.id === "form_search")) {

									continue row;
								}
							}

							let issue = utils.objectAssign(INVALID_NUMBER_OF_CHILDREN);

							issue.resolve = issue.resolve.replace('+', childElemCount);

							issue.src = rows[r];

							issues.push(issue);

						}
						else if (childElemCount === 2) {

							rowFullColumnsCheck = rows[r].querySelectorAll('.emp-col-full');

							if (rowFullColumnsCheck.length === 2) {

								let issue = utils.objectAssign(TOO_MANY_FULL_COLUMNS);

								issue.src = rows[r];

								issues.push(issue);
							}

						}

						rowChildren:
						for (let rc = 0, rcLen = rChildren.length; rc < rcLen; rc++) {

							let child = rChildren[rc];

							// Filter out all but the actual DOM elements
							if (child.nodeType === 1) {

								switch (child.nodeName) { 

									case 'DIV':

										var allowedContainersClasses = ['emp-col-full', 'emp-col-half', 'emp-search-box-field'];

										var additionalInspectionClasses = ['emp-field'];

										// Start by checking all of the allowed containers

										var childClasses = child.className.split(' ');

										for (var c = 0, cLen = childClasses.length; c < cLen; c++) {

											// Check to see if this is an approved row container
											if (allowedContainersClasses.indexOf(childClasses[c]) !== -1) {

												// This is a approved container, move to the next row child.
												continue rowChildren;
											}

										}

										// This child is not part of the defined list, so lets check the parent history stucture page headers break specific page rules.

										let divParents = utils.elementParents(rows[r], true);

										rowParentCheck:
										for (let p = 0, pLen = divParents.length; p < pLen; p++) {

											let pDivNode = divParents[p];

											if ((pDivNode.tagName === "FORM" && pDivNode.id === "form_tp_info") || (pDivNode.tagName === "FORM" && pDivNode.id === "form_search")) {

												continue rowChildren;
											}
										}

										// So we have removed all the normal containers and the special header div rows, now its time to look into all the additional inspect
										for (var c = 0, cLen = childClasses.length; c < cLen; c++) {

											// Check to see if this is an approved row container
											if (additionalInspectionClasses.indexOf(childClasses[c]) !== -1) {

												switch (additionalInspectionClasses[additionalInspectionClasses.indexOf(childClasses[c])]) {

													case 'emp-field':

														var fieldChildren = child.childNodes;

														let normalInput = true;

														for (var fc = 0, fcLen = fieldChildren.length; fc < fcLen; fc++) {

															if (fieldChildren[fc].nodeType === 1) {

																if (fieldChildren[fc].nodeName === "INPUT" && fieldChildren[fc].getAttribute('type') === "hidden") {

																	normalInput = false;
																}
															}

														}

														let issue = false;

														if (normalInput) {

															issue = utils.objectAssign(INVALID_FIELD_PLACEMENT);
														}
														else {

															issue = utils.objectAssign(HIDDEN_INPUT_IN_ROW);
														}

														issue.src = child;

														issues.push(issue);

														break;

												}
											}

										}

										break;

									// All other elements!
									default:

										// Double check parent contents
										let parents = utils.elementParents(rows[r], true);

										// Check each parent for the current item
										rowParentCheck:
										for (let p = 0, pLen = parents.length; p < pLen; p++) {

											let pNode = parents[p];

											if (pNode.tagName === "FORM" && pNode.id === "form_search") {

												continue rowChildren;
											}
										}

										let issue = utils.objectAssign(INVALID_ROW_ELEMENT);

										let tagName = child.nodeName;

										issue.resolve = issue.resolve.replace('+', tagName);

										issue.src = child;

										issues.push(issue);

										break;

								}
							}
						}
					}
				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
		// Button Row Tests
		{
			name: "Button Row Check",
			test: (verbose, utils) => {

				const MISSING_BUTTON_COLUMN = {
					src: false,
					type: "error",
					id: "missing-button-column",
					issue: "Missing Button Column",
					resolve: "The current button row directly contains one or more buttons. Please wrap these buttons in a column",
					code: "0009",
					error: "Buttons not in button column",
					script: "gridLayout"
				};

				const UKNOWN_ELEMENT_IN_BUTTON_ROW = {
					src: false,
					type: "error",
					id: "unknown-element-in-button-row",
					issue: "Unknown element in button row",
					resolve: "This is an unknown element (+) inside of a button row. Button rows should only contain button columns. Please move this element to the appropriate location or remove completely. Reference page mockup for additional details.",
					code: "0010",
					error: "Unknown element in button row",
					script: "gridLayout"
				};

				const TOO_MANY_BUTTON_CHILDREN = {
					src: false,
					type: "error",
					id: "too-many-button-row-columns",
					issue: "Too many button columns in button row",
					resolve: "Too many children exist in the current button row. There is a hard limit of 2 children (columns) per button row in Empire 2. Current row has + children.",
					code: "0010",
					error: "Unknown element in button row",
					script: "gridLayout"
				};

				const FULL_COL_AND_ANOTHER = {
					src: false,
					type: "error",
					id: "button-col-full-and-another-column",
					issue: "Row contains full column and another column",
					resolve: "This button row has a full button column and another column. A row is only allowed to contain 1 full column or 2 half columns. Please change this stucture to comply with these requirements.",
					code: "0010",
					error: "Row contains full column and another column",
					script: "gridLayout"
				};

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var rows = bodyWrapper.querySelectorAll('.cui-row.emp-button-row');

				console.info("Checking button row contents");

				if (rows.length) {

					for (var r = 0, rLen = rows.length; r < rLen; r++) {

						var rChildren = rows[r].childNodes;

						let rowChildrenCount = 0;
						let rowChildrenElem = [];

						for (let rc = 0, rcLen = rChildren.length; rc < rcLen; rc++) {

							let node = rChildren[rc];

							if (node.nodeType === 1) {

								rowChildrenCount++;

								rowChildrenElem.push(node);
							}
						}
						
						if (rowChildrenCount > 0) {

							// Deeper look at the contents.
							for (let re = 0, reLen = rowChildrenElem.length; re < reLen; re++) {

								if (rowChildrenElem[re].nodeName !== "DIV") {

									// Is it just a button?
									if (rowChildrenElem[re].nodeName === "BUTTON") {

										let issue = utils.objectAssign(MISSING_BUTTON_COLUMN);

										issue.src = rows[r];

										issues.push(issue);
									}
									else {

										// Unknown element
										let issue = utils.objectAssign(UKNOWN_ELEMENT_IN_BUTTON_ROW);

										issue.src = rowChildrenElem[re];

										issue.resolve = issue.resolve.replace('+', rowChildrenElem[re].nodeName);

										issues.push(issue);
									}
								}
								else {

									// Checks to make sure the div is a button-col div or a emp-field. Otherwise throw an error.
									if (!rowChildrenElem[re].classList.contains('button-col')) {

										// Unknown element
										let issue = utils.objectAssign(UKNOWN_ELEMENT_IN_BUTTON_ROW);

										issue.src = rowChildrenElem[re];

										issue.resolve = issue.resolve.replace('+', rowChildrenElem[re].nodeName);

										issues.push(issue);
										
									}

								}
							}

							// Next verify the number of button columns in the row.
							let buttonCols = rows[r].querySelectorAll('button-col');

							if (buttonCols.length > 2) {

								let issue = utils.objectAssign(TOO_MANY_BUTTON_CHILDREN);

								issue.src = row[r];

								issue.resolve = issue.resolve.replace('+', buttonCols.length);

								issues.push(issue);
							}
							else if (buttonCols.length === 2) {

								if (buttonCols[0].classList.contains('emp-col-full') || buttonCols[1].classList.contains('emp-col-full')) {

									let issue = utils.objectAssign(FULL_COL_AND_ANOTHER);

									issue.src = row[r];

									issues.push(issue);

								}

							}

						}

					}
				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
		// Column Test - Check to make sure specific elements are not inside of the column because many different things can exist inside
		{
			name: "Column Check",
			test: (verbose, utils) => {

				const SECTION_IN_COLUMN = {
					src: false,
					type: "error",
					id: "section-in-column",
					issue: "Group in Grid Column",
					resolve: "Groups should not be found inside of a column. Groups act similar to row, and provide there own structure. If you need more than one group to be side by side (sit on the same line) add the <em>inline-group<em> style attribute on the E2:Group tag. and place the E2:Group next to each other in the appropriate order.",
					code: "0004",
					error: "Content not inside expected grid layout",
					script: "gridLayout"
				};

				const ROW_IN_COLUMN = {
					src: false,
					type: "error",
					id: "row-in-column",
					issue: "Grid Row in Grid Column",
					resolve: "Grid column contains grid row. remove this row as Empire does not support rows in columns.",
					code: "0005",
					error: "Content not inside expected grid layout",
					script: "gridLayout"
				};

				const COLUMN_IN_COLUMN = {
					src: false,
					type: "error",
					id: "column-in-column",
					issue: "Nested Grid Column in Column",
					resolve: "An additional grid column apprears inside the current grid column. Remove this redunancy.",
					code: "0006",
					error: "Content not inside expected grid layout",
					script: "gridLayout"
				};

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var columns = bodyWrapper.querySelectorAll('.emp-col-half, .emp-col-full');

				console.info("Checking column contents");

				if (columns.length) {

					columns:
					for (var c = 0, cLen = columns.length; c < cLen; c++) {

						var invalidChildrenElements = ['section', 'div.cui-row', 'div.emp-col-full, div.emp-col-half'];

						for (let ice = 0, iceLen = invalidChildrenElements.length; ice < iceLen; ice++) {

							let columnChildren = columns[c].querySelectorAll(invalidChildrenElements[ice]);

							if (columnChildren.length) {

								let issue = false;

								switch(invalidChildrenElements[ice]) {

									case 'section':

										issue = utils.objectAssign(SECTION_IN_COLUMN);

										issue.src = columns[c];

										break;

									case 'div.cui-row':

										issue = utils.objectAssign(ROW_IN_COLUMN);

										issue.src = columns[c];

										break;

									case 'div.emp-col-full, div.emp-col-half':
									case 'div.emp-col-full':
									case 'div.emp-col-half':

										issue = utils.objectAssign(COLUMN_IN_COLUMN);

										issue.src = columns[c];

										break;

								}

								issues.push(issue);

							}

						}

					}

				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		}
	]
}