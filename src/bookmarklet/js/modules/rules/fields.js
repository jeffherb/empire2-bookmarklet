export default {
	"name": "fields",
	"description": "Multipe point test to verify that rows, sections and tables appear in the correct grid context.",
	"context": "page",
	"test": [
		// Numeric Fields
		{
			name: "Numeric Alignment",
			test: (verbose, utils) => {

				const NUMERIC_ALIGNMENT = {
					src: false,
					type: "error",
					id: "numeric_alignment",
					issue: "Numeric values should be right aligned",
					resolve: 'Numeric field (+) should appear right aligned. Please add the style="*"',
					code: "0000",
					error: "Number Not Right Aligned",
					script: "field"
                };
                
                var numericLabelIndicators = ['$', '%', '#', 'count'];

				var issues = [];

                var bodyWrapper = document.getElementById('body-wrapper');
                
                var fields = bodyWrapper.querySelectorAll('.emp-field');

				var rows = bodyWrapper.querySelectorAll('.cui-row');

				console.info("Checking for numeric fields and there alignment classes");

				if (fields.length) {

                    fields:
                    for (var f = 0, fLen = fields.length; f < fLen; f++) {

                        var numericField = false;

                        // Get the data attribute data-store-id
                        var dataStore = fields[f].getAttribute('data-store-id');

                        if (dataStore) {

                            var fieldDS = emp.ds.getStore(dataStore);

                            if (fieldDS.label && fieldDS.input && fieldDS.type === "text") {
                                
                                var fieldOrgLabel = fieldDS.label.text;
                                var fieldLabel = fieldDS.label.text.toLowerCase();
                                var matchingIndicator = false;

                                indicatorTest:
                                for (var i = 0, iLen = numericLabelIndicators.length; i < iLen; i++) {

                                    if (fieldLabel.indexOf(numericLabelIndicators[i]) !== -1) {

                                        numericField = true;

                                        matchingIndicator = numericLabelIndicators[i];

                                        break indicatorTest;
                                    }

                                }

                                if (numericField) {

                                    if (!fieldDS.input.style || (fieldDS.input.style.indexOf('currency') === -1 && fieldDS.input.style.indexOf('align-right') === -1)) {

                                        let issue = utils.objectAssign(NUMERIC_ALIGNMENT);

                                        issue.resolve = issue.resolve.replace('+', fieldOrgLabel);

                                        if (matchingIndicator === "$") {

                                            issue.resolve = issue.resolve.replace('*', "currency");
                                        }
                                        else {
                                            issue.resolve = issue.resolve.replace('*', "align-right");
                                        }

                                        issue.src = fields[f];
    
                                        issues.push(issue);
                                    }

                                }

                            }

                        }

                    }

                }

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
	]
}