export default {
	"name": "duplicates",
	"description": "Check for duplicate id and name values",
	"context": "page",
	"test": [
		// Duplicate IDs Test
		{
			name: "Duplicate ID's",
			test: (verbose, utils) => {

				const DUPLICATE_ID = {
					src: false,
					type: "error",
					id: "empty_row",
					issue: "Duplicate ID",
					resolve: "Two or more elements are sharing the same ID (+). Review your viewkeys and use the proper suffix and prefix as needed.",
					code: "0000",
					error: "Duplicate ID",
					script: "duplicates"
				};

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var ids = bodyWrapper.querySelectorAll("[id]");

				console.info("Checking for duplicate ID's");

				if (ids.length) {

					var totalStore = {};

					for (var i = 0, iLen = ids.length; i < iLen; i++) {

						var id = ids[i].getAttribute('id');

						if (!totalStore[id]) {
							totalStore[id] = 1;
						}
						else {
							totalStore[id] = totalStore[id] + 1;
						}

					}

					invalidIDs = [];

					for (var id in totalStore) {

						if (totalStore[id] > 1) {
							invalidIDs.push(id);
						}
					}

					if (invalidIDs.length) {

						for (let is = 0, isLen = invalidIDs.length; is < isLen; is++) {

							let issue = utils.objectAssign(DUPLICATE_ID);

							issue.resolve = issue.resolve.replace('+', invalidIDs[is]);

							issues.push(issue);
						}
					}

				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
	]
}