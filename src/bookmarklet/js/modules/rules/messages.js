export default {
	"name": "messages",
	"description": "Multipe point to find and determine if messages are correct",
	"context": "page",
	"test": [
		// Search Composites
		{
			"name": "Bad Success Messages",
			"test": (verbose, utils) => {

				const INVALID_SUCCESS_MSG = {
					src: false,
					type: "error",
					id: "retrieval-message",
					issue: "Retrieval Successful Not Allowed",
					resolve: 'Empire Standards do not allow for any type of a success message based on retreival of data. Success messages should only be displayed when a transaction of data occures (Record is Updated, Created, Deleted (expired)). Remove this message. Please reach out to the UI Support team for more information or see the Empire Standard Manuel.',
					code: "0000",
					error: "Retrieval Successful Not Allowed",
					script: "mesages"
				}

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var messages = bodyWrapper.querySelectorAll('.cui-success');

				console.info("Checking page messages - success messages");

                if (messages.length) {

                    for (var m = 0, mLen = messages.length; m < mLen; m++) {

                        let msg = messages[m];
                        let msgText = msg.textContent;

                        if (msgText === "Retrieval Successful") {

                            let issue = utils.objectAssign(INVALID_SUCCESS_MSG);

                            issue.src = msg;

                            issues.push(issue);
                        }

                    }
                }

				if (issues.length) {

					return issues;
				}

				return false;
			}
		}
	]
}