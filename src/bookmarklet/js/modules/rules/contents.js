export default {
	"name": "contents",
	"description": "Multipe point check script for specific sections of the page that are not across all empire pages.",
	"context": "page",
	"test": [
		// Search Composites
		{
			"name": "As of Date Format",
			"test": (verbose, utils) => {

				const INVALID_AS_OF_DATE_FORMAT = {
					src: false,
					type: "error",
					id: "column-in-column",
					issue: "Search Composite not build using E2:Composite Tag",
					resolve: 'It appears you have a standard input and a button inside the same column. When these two elements share the same column and are related, they should be built using the &lt;E2:Composite template="search"&gt; custom tag to achive the appropriate layout.',
					code: "0000",
					error: "Search Composite Not in Composite Structure",
					script: "column"
				}

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var asofSection = bodyWrapper.querySelectorAll('#form_asof');

				console.info("Checking Contents - As Of");

				if (asofSection) {

				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		}
	]
}