export default {
	"name": "column",
	"description": "Multipe point to attempt to find some of the most common problems with column contents",
	"context": "page",
	"test": [
		// Search Composites
		{
			"name": "Attempted Search Composite",
			"test": (verbose, utils) => {

				const INVALID_SEARCH_COMPOSITE = {
					src: false,
					type: "error",
					id: "column-in-column",
					issue: "Search Composite not build using E2:Composite Tag",
					resolve: 'It appears you have a standard input and a button inside the same column. When these two elements share the same column and are related, they should be built using the &lt;E2:Composite template="search"&gt; custom tag to achive the appropriate layout.',
					code: "0000",
					error: "Search Composite Not in Composite Structure",
					script: "column"
				}

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var columns = bodyWrapper.querySelectorAll('.emp-col-full, .emp-col-half');

				console.info("Checking column contents - search composites");

				for (var c = 0, cLen = columns.length; c < cLen; c++) {

					let inputs = columns[c].querySelectorAll('input, select, textarea');
					let readOnlySpans = columns[c].querySelectorAll('emp-field span.emp-data');
					let buttons = columns[c].querySelectorAll('button');
					let hiddenInputs = columns[c].querySelectorAll('input[type="hidden"]');

					// Figure out the real input count (need to remove hidden inputs);
					let inputCount = inputs.length - hiddenInputs.length;

					if ((inputCount === 1 || readOnlySpans === 1) && buttons.length === 1 && !buttons[0].classList.contains('cui-c-datepicker') && !buttons[0].classList.contains('emp-icon-section-toggle-collapse')) {

						// Check to see if we can fund the composite class...
						let compositeSearch = columns[c].querySelectorAll('.emp-composite');

						if (compositeSearch.length === 0) {

							// No composite was found, error out!
							let issue = utils.objectAssign(INVALID_SEARCH_COMPOSITE);

							issue.src = columns[c];

							issues.push(issue);

						}

					}

				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		}
	]
}