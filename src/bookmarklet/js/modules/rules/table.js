export default {
	"name": "table",
	"description": "Multipe point to attempt to find some of the most common problems with tables",
	"context": "page",
	"test": [
		{
			"name": "Table Column Labels",
			"test": (verbose, utils) => {

				const MISSING_LABEL = {
					src: false,
					type: "error",
					id: "column-label-missing",
					issue: "Table column header is missing label text",
					resolve: 'Column + is missing its table label text. Add the label attribute to the E2:TableColumn tag and set it to the correct value. See page mockup for more details.',
					code: "0000",
					error: "Column Missing Label Text",
					script: "table"
				}

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var tables = bodyWrapper.querySelectorAll('table');

				console.info("Checking tables - column labels");

				if (tables.length) {

					for (var t = 0, tLen = tables.length; t < tLen; t++) {

						let table = tables[t];
						let tableId = table.getAttribute('id');

						let dataStore = emp.reference.tables[tableId].dataStore;

						for (let h = 0, hLen = dataStore.head.rows[0].columns.length; h < hLen; h++) {

							let header = dataStore.head.rows[0].columns[h];

							if (!header.text || header.text.trim() === "") {

								let issue = utils.objectAssign(MISSING_LABEL);

								issue.src = table;

								issue.resolve = issue.resolve.replace('+', h + 1);

								issues.push(issue);
							}
						}
					}
				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
		{
			"name": "Table Column Data-type",
			"test": (verbose, utils) => {

				const UNDEFINED_COLUMN_TYPE = {
					src: false,
					type: "warning",
					id: "column-data-type-missing",
					issue: "Table column data-type is undefined",
					resolve: 'The + column is missing the column-type attributes. By default it has been set to "alpha", but this might not be a correct representation of the contents of the column and may affect how the column is sorted. Use E2:TableColumn columnType attribute to specify the type of column',
					code: "0001",
					error: "Column Data-type is Undefined",
					script: "table"
				}

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var tables = bodyWrapper.querySelectorAll('table');

				console.info("Checking tables - column data-type undefined");

				if (tables.length) {

					for (var t = 0, tLen = tables.length; t < tLen; t++) {

						let table = tables[t];
						let tableId = table.getAttribute('id');

						let dataStore = emp.reference.tables[tableId].dataStore;

						for (let h = 0, hLen = dataStore.head.rows[0].columns.length; h < hLen; h++) {

							let header = dataStore.head.rows[0].columns[h];
							let headerAttributes = header.attributes;

							if (!headerAttributes.hasOwnProperty('data-type') && (!header.visibility || header.visibility !== "hidden")) {

								let issue = utils.objectAssign(UNDEFINED_COLUMN_TYPE);

								issue.src = table;

								issue.resolve = issue.resolve.replace('+', header.text.trim());

								issues.push(issue);
							}

						}
					}
				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
		{
			"name": "Table Buttom Menu Render Check",
			"test": (verbose, utils) => {

				const INVALID_BUTTON_MENU_SPEC = {
					src: false,
					type: "warning",
					id: "invalid-button-menu-spec",
					issue: "Table Menus are rendering using a fallback method.",
					resolve: 'This table contains a button menu, but the buttons are not being rendered inside of a <e2:TableColumn columnType="buttonMenu">. Please convert this as soon as possible.',
					code: "0002",
					error: "Table Menus Rendered Using Old Method",
					script: "table"
				};

				var issues = [];

				console.info("Checking tables - button menu render method");

				var tableRefs = emp.reference.tables;

				for (var table in tableRefs) {

					let dataStore = emp.reference.tables[table].dataStore;

					if (dataStore.metadata && dataStore.metadata.renderButtonMenu) {

						let issue = utils.objectAssign(INVALID_BUTTON_MENU_SPEC);

						issue.src = document.getElementById(dataStore.attributes.id);

						issues.push(issue); 
					}
				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
		{
			"name": "Table Column Data-type evaluation - dates",
			"test": (verbose, utils) => {

				const NOT_DATE_DATA_TYPE = {
					src: false,
					type: "warning",
					id: "column-not-date-type",
					issue: 'Table column detected to be possible "date" column-type',
					resolve: 'The + column appears to have the wrong data-type attribute or its missing completely. Because this column title either has the word "Date" or it contains "Period Begin/End" or "Range Begin/End". If this is a date column be sure to set the dataType attribute to date on the e2:TableColumn tag.',
					code: "0003",
					error: "Possible Column-Type Mismatch",
					script: "table"
				};

				const NOT_CURRENCY_DATA_TYPE = {
					src: false,
					type: "warning",
					id: "column-not-curreny-type",
					issue: 'Table column detected to be possible "currency" column-type',
					resolve: 'The + column appears to have the wrong column-type attribute or its missing completely. Because this column contained the "$" (Dollar Symbol) it is believed to be a currency column. If this is a currency column be sure to set the columnType attribute to currency on the e2:TableColumn tag.',
					code: "0004",
					error: "Possible Column-Type Mismatch",
					script: "table"
				};

				const NOT_NOTIFIER_DATA_TYPE = {
					src: false,
					type: "warning",
					id: "column-not-notifier-type",
					issue: 'Table column detected to be possible "notifier" column-type',
					resolve: 'The + column appears to have the wrong column-type attribute or its missing completely. Because this table has a notifier found in the table body, it is believed to be a notifier column. If this is a notifier column be sure to set the columnType attribute to notifier on the e2:TableColumn tag.',
					code: "0005",
					error: "Possible Column-Type Mismatch",
					script: "table"
				};

				const NOT_BUTTON_DATA_TYPE = {
					src: false,
					type: "warning",
					id: "column-not-button-type",
					issue: 'Table column detected to be possible "button" column-type',
					resolve: 'The + column appears to have the wrong column-type attribute or its missing completely. Because this table has a button found in the table body, it is believed to be a button column. If this is a button column be sure to set the columnType attribute to button on the e2:TableColumn tag.',
					code: "0006",
					error: "Possible Column-Type Mismatch",
					script: "table"
				};

				var issues = [];

				var bodyWrapper = document.getElementById('body-wrapper');

				var tables = bodyWrapper.querySelectorAll('table');

				console.info("Checking tables - column Column-Type evaluation");

				// Though we say we are looking at the different column-type, in our code we are looking at the data-type attribute part of the JSON.

				if (tables.length) {

					for (var t = 0, tLen = tables.length; t < tLen; t++) {

						let table = tables[t];
						let tableID = table.getAttribute('id');

						let dataStore = emp.reference.tables[tableID].dataStore;

						rootLoop:
						for (var h = 0, hLen = dataStore.head.rows[0].columns.length; h < hLen; h++) {

							let header = dataStore.head.rows[0].columns[h];
							let headerAttributes = header.attributes;

							// Only continue if this is a visible column.
							if (!header.visibility || header.visibility !== "hidden") {

								let headerText = header.text.trim().toLowerCase();

								let dateTitles = ['period begin', 'period end', 'range begin', 'range end'];

								// Check to see if the text in the header contains the word "data" as we only do this for date columns
								if (/\bdate\b/.test(headerText)) {

									if (!headerAttributes['data-type'] || headerAttributes['data-type'] !== 'date') {

										let issue = utils.objectAssign(NOT_DATE_DATA_TYPE);

										issue.src = table;

										issue.resolve = issue.resolve.replace('+', header.text.trim());

										issues.push(issue);

										continue rootLoop;
									}
								}
								// Check for special date header labels like liability period begin/end as they are also date fields.
								else if (dateTitles.indexOf(headerText) !== -1) {

									if (!headerAttributes['data-type'] || headerAttributes['data-type'] !== 'date') {

										let issue = utils.objectAssign(NOT_DATE_DATA_TYPE);

										issue.src = table;

										issue.resolve = issue.resolve.replace('+', header.text.trim());

										issues.push(issue);

										continue rootLoop;
									}
								}
								// Check to see if the header text contains the "$" character which represents currency
								else if (headerText.indexOf('$') !== -1) {

									if (!headerAttributes['data-type'] || headerAttributes['data-type'] !== 'currency') {

										let issue = utils.objectAssign(NOT_CURRENCY_DATA_TYPE);

										issue.src = table;

										issue.resolve = issue.resolve.replace('+', header.text.trim());

										issues.push(issue);

										continue rootLoop;
									}

								}
								else {

									// At this point we can only check columns that do not have a specified columnType (data-type in JSON) or are set to alpha as they may be incorrect based on what we find in the table.
									// We only do these tests if the body actually has some length

									if ((!headerAttributes['data-type'] || headerAttributes['data-type'] === 'alpha') && dataStore.body && dataStore.body.rows && dataStore.body.rows.length) {

										rowLoop:
										for (let r = 0, rLen = dataStore.body.rows.length; r < rLen; r++) {

											let row = dataStore.body.rows[r];
											let column = row.columns[h];

											if (column.contents && column.contents.length) {

												contents:
												for (let c = 0, cLen = column.contents.length; c < cLen; c++) {

													if (column.contents[c].template) {

														switch (column.contents[c].template) {

															case 'notifier':

																let issue = utils.objectAssign(NOT_NOTIFIER_DATA_TYPE);

																issue.src = table;

																issue.resolve = issue.resolve.replace('+', header.text.trim());

																issues.push(issue);

																break;

															case 'field':

																if (column.contents[c].type === "button") {

																	let issue = utils.objectAssign(NOT_BUTTON_DATA_TYPE);

																	issue.src = table;

																	issue.resolve = issue.resolve.replace('+', header.text.trim());

																	issues.push(issue);

																}
																else if (column.contents[c].type === "date") {

																	let issue = utils.objectAssign(NOT_DATE_DATA_TYPE);

																	issue.src = table;

																	issue.resolve = issue.resolve.replace('+', header.text.trim());

																	issues.push(issue);

																}

																break;

														}

														continue rootLoop;

													}

												}

											}

											continue;;

										}

									}

								}

							}

						}

					}

				}

				if (issues.length) {

					return issues;
				}

				return false;
			}
		},
	]
}