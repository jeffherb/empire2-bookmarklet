function createElement(elmString, attrObj, text) {

	var elem = document.createElement(elmString);

	if (typeof attrObj === "object") {
		setAttribute(elem, attrObj);
	}

	if (typeof text === "string" || typeof text === "number") {
		setElementText(elem, text);
	}

	return elem;
};

function setAttribute(elem, attrObj) {

	for (var attr in attrObj) {

		elem.setAttribute(attr, attrObj[attr]);
	}

};

function setElementText(elem, text) {

	var textNode = document.createTextNode(text);

	elem.appendChild(textNode);
};

// Classes
var CLASSES = {
	mainPanel: "eb-main-panel",
	mainWrapperClass: 'eb-main-wrapper',
	mainPanelClose: "eb-close-control",
	returnHome: 'eb-home-control',
	header: "eb-header",
	statusPanel: 'eb-status-panel',
	countWrapper: 'eb-count-wrapper',
	controlWrapper: 'eb-control-wrapper',
	bigNumbers: 'eb-big-text-numbers',
	countLabel: 'eb-count-label',
	warningBox: 'eb-warning-box',
	viewAllIssuesButton: 'eb-view-all-issues',
	countBox: 'eb-count-box',
	errorBox: 'eb-error-box',
	boxText: 'eb-box-text',

	// Issues Panel
	issuePanel: 'eb-issues-panel',
	issueDefectBar: 'eb-issue-bar',
	issueIcon: 'eb-issue-icon',
	issueName: 'eb-issue-name',
	issueResolveContainer: 'eb-issue-resolve-container',
	issueResolveLabel: 'eb-issue-resolve-label',
	issueResolveMsg: 'eb-issues-resolve-msg',
	issueLabel: 'eb-issue-label',
	issueDescription: 'eb-issue-description',

	issuePagerContainer: 'eb-issues-pager-container',
	issuePageNumber: 'eb-issues-page-number',
	issueNext: 'eb-issue-next',
	issuePrevious: 'eb-issue-previous',

	// Success Panel
	successWrapper: 'eb-success-wrapper',
	successIcon: 'eb-success-icon',
	successText: 'eb-success-text',

	// Error Box
	elemArrowErrorBox: 'eb-element-error-box',

	// transition and state styles
	hide: 'eb-hide-panel',
	fadeOut: 'eb-fade-out',
	fadeIn: 'eb-fade-in',
	visible: 'eb-visibility',
	noVisible: 'eb-no-visibility',
	scaleSize: 'eb-scaleSize',
	largePanel: 'eb-large-panel',

	// Icons
	warningIcon: 'eb-warning-icon',
	errorIcon: 'eb-error-icon'
};

export default class Panel {

	constructor(testResults) {

		this.data = testResults;

		// Important reference nodes
		this.mainPanel = false;
		this.closeControl = false;
		this.statusPanel = false;
		this.issuePanel = false;

		this.issueExist = true;

		// Object to track current issue context
		this.currentIssue = {
			testIndex: false,
			isseuIndex: false,
			objects: {
				test: false,
				issue: false
			}
		};

		// Call the init method;
		this.init();
	};

	showPanel(instance) {

		instance.mainPanel.classList.remove('eb-hide-panel');

		if (!instance.arrowBoxVisibility && instance.arrowBox) {

			instance.showArrowBox(instance.currentIssue);
		}
	}

	hidePanel(instance) {

		instance.mainPanel.classList.add('eb-hide-panel');

		if (instance.arrowBoxVisibility && instance.arrowBox) {

			instance.hideArrowBox();
		}
	};

	backToHome(instance, source) {

		instance.transitionTo(instance, 'status', this.currentScreen);
	}

	transitionTo(instance, target, source) {

		var mainPanel = instance.mainPanel;

		var origTarget = target;
		var origSource = source;

		if (origTarget !== "status") {

			instance.homeControl.classList.remove(CLASSES.hide);

			//if (instance)
		}
		else {

			instance.homeControl.classList.add(CLASSES.hide);

			if (instance.arrowBoxVisibility && instance.arrowBox) {

				instance.hideArrowBox();
			}
		}

		if (origTarget === "issue" && !instance.currentIssue.testIndex) {
			this.populateNextIssue(instance);
		}

		switch (source) {

			case "status":
				source = instance.statusPanel;
				break;

			case "issue":
				source = instance.issuePanel;
				break;
		};

		switch (target) {

			case "status": 
				target = instance.statusPanel;
				break;

			case "issue":
				target = instance.issuePanel;
				break;
		}

		source.classList.add(CLASSES.visible);
		source.classList.add(CLASSES.fadeOut);

		setTimeout(() => {

			source.classList.add(CLASSES.hide);
			source.classList.remove(CLASSES.visible);
			source.classList.remove(CLASSES.fadeOut);

			if (origTarget === "status") {

				mainPanel.classList.remove(CLASSES.largePanel);
			}
			else {
				
				mainPanel.classList.add(CLASSES.largePanel);
			}

			mainPanel.classList.add(CLASSES.scaleSize);

			setTimeout(() => {

				target.classList.add(CLASSES.noVisible);
				target.classList.remove(CLASSES.hide);
				target.classList.add(CLASSES.fadeIn);

				instance.currentScreen = origTarget;

			}, 200);

		}, 100);
	};

	cleanLastIssue() {
	};

	setupErrorBox(issueObj) {

		function elementPosAndSize(element) {

		    var de = document.documentElement;
		    var box = element.getBoundingClientRect();
		    var top = box.top + window.pageYOffset - de.clientTop;
		    var left = box.left + window.pageXOffset - de.clientLeft;
		    var height = element.clientHeight;
		    var width = element.clientWidth;
		    var fullWidth = (document.body.clientWidth === width) ? true : false;

		    return { top: top, left: left, height:height, width: width, fullWidth: fullWidth };
		}

		var visibility = issueObj.src.offsetParent;

		var boxOffsets = {
			top: 3,
			bottom: 3,
			left: 5,
			right: 5
		};

		if (visibility !== undefined && visibility !== null) {

			var elemPosnSize = elementPosAndSize(issueObj.src);

			var arrowPosnSize = {
				top: Math.round(elemPosnSize.top) - boxOffsets.top,
				height: Math.round(elemPosnSize.height) + boxOffsets.bottom
			};

			if (elemPosnSize.fullWidth) {

				arrowPosnSize.left = Math.round(elemPosnSize.left) + (boxOffsets.left + boxOffsets.right);
				arrowPosnSize.width = Math.round(elemPosnSize.width) - ((boxOffsets.left + boxOffsets.right) * 2);

			}
			else {

				arrowPosnSize.top = Math.round(elemPosnSize.top) - 5;
				arrowPosnSize.height = Math.round(elemPosnSize.height) + 10;
				arrowPosnSize.left = Math.round(elemPosnSize.left) - 5;
				arrowPosnSize.width = Math.round(elemPosnSize.width) + (boxOffsets.left + boxOffsets.right);
			}

			this.arrowBox.style.top = arrowPosnSize.top + "px";
			this.arrowBox.style.left = arrowPosnSize.left + "px";
			this.arrowBox.style.height = arrowPosnSize.height + "px";
			this.arrowBox.style.width = arrowPosnSize.width + "px";

			if (this.arrowBox.classList.contains(CLASSES.hide)) {
				this.arrowBox.classList.remove(CLASSES.hide);
			}

			this.arrowBoxVisibility = true;

			var boxScrollTop = arrowPosnSize.top - 100;

			if (boxScrollTop < 0) {
				boxScrollTop = 0;
			}

			window.scrollTo(0, boxScrollTop);
		}
	};

	showArrowBox() {

		if (!this.arrowBoxVisibility) {

			this.arrowBoxVisibility = true;

			if (this.arrowBox.classList.contains(CLASSES.hide)) {

				this.arrowBox.classList.remove(CLASSES.hide);
			}

		}
	};

	hideArrowBox() {

		if (this.arrowBoxVisibility) {

			this.arrowBoxVisibility = false;

			this.arrowBox.classList.add(CLASSES.hide);
		}
	};

	populateNextIssue(direction) {

		function findTestIndex(currentIndex, allTests, resultIndex,  direction) {

			function findIssuePage(newTI, newII) {

				var pageIndex = 0;

				var resultsKeyIndexs = Object.keys(resultIndex);

				var pageCounter = 0;

				for (let tIndex = 0, tLen = resultsKeyIndexs.length; tIndex < tLen; tIndex++) {

					// Only continue if the index is less than ore equial to the new index
					if (tIndex <= newTI) {

						// Check if we are on the end test, if not just sum up the issues count
						if (newTI !== tIndex) {

							pageCounter += allTests[resultIndex[tIndex]].results.length;

						}
						else {

							// Wr are on the test index
							pageCounter += newII + 1;

						}

					}

				}

				return pageCounter;
			}

			var totalIndexCount = Object.keys(resultIndex).length;

			let newTestIndex = false;
			let newIssueIndex = false;
			let newPageIndex = false;

			if (direction === "next") {

				if (currentIndex < (totalIndexCount -1)) {

					newTestIndex = currentIndex + 1;
				}
				else if (currentIndex === (totalIndexCount -1)) {

					newTestIndex = 0;
				}

				newIssueIndex = 0;

				return {
					testIndex: newTestIndex,
					issueIndex: newIssueIndex,
					pageNumber: findIssuePage(newTestIndex, newIssueIndex)
				};

			}
			else {

				if (currentIndex > 0) {

					newTestIndex = currentIndex - 1;
				}
				else if (currentIndex === 0) {

					newTestIndex = totalIndexCount - 1;
				}

				newIssueIndex = allTests[resultIndex[newTestIndex]].results.length - 1;

				return {
					testIndex: newTestIndex,
					issueIndex: newIssueIndex,
					pageNumber: findIssuePage(newTestIndex, newIssueIndex)
				}

			}
		};

		if (this.currentIssue.testIndex !== false) {

			// Based on the current index see what we need to do
			let currentTestIndex = this.currentIssue.testIndex;
			let currentIssueIndex = this.currentIssue.issueIndex;

			let currentTest = this.data.results[this.resultIndex[currentTestIndex]];

			if (direction === "next") {

				// Check to see if the current test and result index is not the last
				if (currentIssueIndex < (currentTest.results.length) - 1) {

					// Just update the issue index for the current test
					this.currentIssue.issueIndex = currentIssueIndex + 1;
					this.currentIssue.pageNumber = this.currentIssue.pageNumber + 1;

				}
				else {

					let newTestIndexObj = findTestIndex(currentTestIndex, this.data.results, this.resultIndex, "next");

					this.currentIssue.testIndex = newTestIndexObj.testIndex;
					this.currentIssue.issueIndex = newTestIndexObj.issueIndex;
					this.currentIssue.pageNumber = newTestIndexObj.pageNumber;

				}

			}
			else {

				if (currentIssueIndex > 0) {

					// Just update the issue index for the current test
					this.currentIssue.issueIndex = currentIssueIndex - 1;
					this.currentIssue.pageNumber = this.currentIssue.pageNumber - 1;
				}
				else {

					let newTestIndexObj = findTestIndex(currentTestIndex, this.data.results, this.resultIndex, "previous");

					this.currentIssue.testIndex = newTestIndexObj.testIndex;
					this.currentIssue.issueIndex = newTestIndexObj.issueIndex;
					this.currentIssue.pageNumber = newTestIndexObj.pageNumber;

				}
			}

		}
		else {

			this.currentIssue.testIndex = 0;
			this.currentIssue.issueIndex = 0;
			this.currentIssue.pageNumber = 1;
		}

		// Pull the date out accordinly
		let testObject = this.data.results[this.resultIndex[this.currentIssue.testIndex]];

		let issueObject = testObject.results[this.currentIssue.issueIndex];

		switch (issueObject.type) {

			case "error":

				if (this.issueIcon.classList.contains(CLASSES.warningIcon)) {
					this.issueIcon.classList.remove(CLASSES.warningIcon);
				}

				this.issueIcon.classList.add(CLASSES.errorIcon);

				break;

			case "warning":

				if (this.issueIcon.classList.contains(CLASSES.errorIcon)) {
					this.issueIcon.classList.remove(CLASSES.errorIcon);
				}

				this.issueIcon.classList.add(CLASSES.warningIcon);
		}

		// Set the test and script name
		this.issueName.innerHTML = issueObject.error;
		this.issueDescription.innerHTML = issueObject.issue;
		this.issueRecommendation.innerHTML = issueObject.resolve;

		this.issuePageNumber.innerHTML = this.currentIssue.pageNumber + "/" + this.totalIssuesCount;


		if (issueObject.src) {

			this.setupErrorBox(issueObject);
		}
		else {

			this.hideArrowBox();
		}

		this.currentIssue.objects.test = testObject;
		this.currentIssue.objects.objects = issueObject;
	};

	createUI(perfectPage) {

		// ============================================================
		// ========= MAIN PANE ======================================
		// ============================================================

		// Extend main panel attributes
		var mainPanel = createElement('div', {
			'id':'empireBookmarkletPanel',
			'class': CLASSES.mainPanel
		});

		var mainWrapper = createElement('div', {
			'class': CLASSES.mainWrapperClass
		});

		// Header
		// ===========================================================

		var header = createElement('header', {
			'class': CLASSES.header
		}, "Empire Test Bookmarklet");

		// Extend main close control attributes
		var mainClose = createElement('a', {
			'id': CLASSES.mainPanelClose,
			'class': CLASSES.mainPanelClose,
			'title': "Close",
			'alt': "Close",
			"role": "button"
		});

		var homeControl = createElement('a', {
			'id': CLASSES.returnHome,
			'class': CLASSES.returnHome + ' ' + CLASSES.hide,
			'title': "Return to Home",
			'alt': "Return to Home",
			'role': "button"
		});

		mainClose.addEventListener('click', () => {

			this.hidePanel(this);
		}, false);

		homeControl.addEventListener('click', () => {

			this.backToHome(this);

		}, false);

		header.appendChild(homeControl)
		header.appendChild(mainClose);

		// Main Panel 
		mainWrapper.appendChild(header);

		if (!perfectPage) {

			var statusPanel = createElement('div', {
				'id': CLASSES.statusPanel,
				'class': CLASSES.statusPanel
			});

			mainWrapper.appendChild(statusPanel);

			// Counter Box!
			// ============================================================

			var countWrapper = createElement('div', {
				'class': CLASSES.countWrapper
			});

			// Warning box
			var warningBox = createElement('div', {
				'id': CLASSES.warningBox,
				'class': CLASSES.warningBox + " " + CLASSES.countBox
			});

			var warningCount = createElement('span', {
				'class': CLASSES.boxText + " " + CLASSES.bigNumbers
			}, this.data.warnings);

			var warningLabel = createElement('span', {
				'class': CLASSES.boxText + " " + CLASSES.countLabel
			}, "Warnings");


			warningBox.appendChild(warningCount);
			warningBox.appendChild(warningLabel);

			// Error Box
			var errorBox = createElement('div', {
				'id': CLASSES.errorBox,
				'class': CLASSES.errorBox + " " + CLASSES.countBox
			});

			var errorCount = createElement('span', {
				'class': CLASSES.boxText + " " + CLASSES.bigNumbers
			}, this.data.errors);

			var errorLabel = createElement('span', {
				'class': CLASSES.boxText + " " + CLASSES.countLabel
			}, "Errors");

			errorBox.appendChild(errorCount);
			errorBox.appendChild(errorLabel);

			// Add the count boxes to the wrapper
			countWrapper.appendChild(warningBox);
			countWrapper.appendChild(errorBox);

			// Control Box!
			// ============================================================

			var controlWrapper = createElement('div', {
				'class': CLASSES.controlWrapper,
			});

			var stepThroughButton = createElement('button',{
				'role': "button",
				'class': CLASSES.viewAllIssuesButton
			}, "View All Issues");

			stepThroughButton.addEventListener('click', () => {

				this.transitionTo(this, 'issue', 'status');

			}, false);

			controlWrapper.appendChild(stepThroughButton);

			// ============================================================
			// ========= ISSUES PANE ======================================
			// ============================================================

			// Issue Panel
			var issuePanel = createElement('div', {
				'id': 'eb-issues-panel',
				'class': CLASSES.issuePanel + " " + CLASSES.hide
			});

			var issueBar = createElement('div', {
				'class': CLASSES.issueDefectBar	
			});

			var issueIcon = createElement('span', {
				'class': CLASSES.issueIcon
			});

			var issueName = createElement('span', {
				'class': CLASSES.issueName
			});

			issueBar.appendChild(issueIcon);
			issueBar.appendChild(issueName);

			var issueResolveContainer = createElement('div', {
				'class': CLASSES.issueResolveContainer
			});

			var issueLabel = createElement('span', {
				'class': CLASSES.issueLabel
			}, "Issue:");

			var issueDescription = createElement('span', {
				'class': CLASSES.issueDescription
			});

			var issueResolveLabel = createElement('span', {
				'class': CLASSES.issueResolveLabel
			}, "Recommendation:");

			var issueResolveMsg = createElement('span', {
				'class': CLASSES.issueResolveMsg
			});

			issueResolveContainer.appendChild(issueLabel);
			issueResolveContainer.appendChild(issueDescription);
			issueResolveContainer.appendChild(issueResolveLabel);
			issueResolveContainer.appendChild(issueResolveMsg);

			var issuePagerContainer = createElement('div', {
				'class': CLASSES.issuePagerContainer
			});

			var issueNext = createElement('button', {
				'class': CLASSES.issueNext
			}, "Next");

			issueNext.addEventListener('click', () => {

				this.populateNextIssue('next');
			});

			var issuePrevious = createElement('button', {
				'class': CLASSES.issuePrevious
			}, "Previous");

			issuePrevious.addEventListener('click', () => {

				this.populateNextIssue('previous');
			});

			var issuePageNumber = createElement('span', {
				'class': CLASSES.issuePageNumber
			});

			issuePagerContainer.appendChild(issuePrevious);
			issuePagerContainer.appendChild(issuePageNumber);
			issuePagerContainer.appendChild(issueNext);

			// Assemble issue panel
			issuePanel.appendChild(issueBar);
			issuePanel.appendChild(issueResolveContainer);
			issuePanel.appendChild(issuePagerContainer);

			// Put everything together
			// ==============================================================

			statusPanel.appendChild(countWrapper);
			statusPanel.appendChild(controlWrapper);

			// Error box
			var arrowBox = createElement('div', {
				'id': CLASSES.elemArrowErrorBox,
				'class': CLASSES.elemArrowErrorBox + " " + CLASSES.hide
			});

			document.body.appendChild(arrowBox);

			mainWrapper.appendChild(issuePanel);

			this.issuePanel = issuePanel;
			this.issueIcon = issueIcon;
			this.issueName = issueName;
			this.issueDescription = issueDescription;
			this.issueRecommendation = issueResolveMsg;
			this.issuePageNumber = issuePageNumber;

			// Arrow Box!
			this.arrowBox = arrowBox;
			this.arrowBoxShown = false;
		}
		else {

			successWrapper = createElement('div', {
				'class': CLASSES.successWrapper
			});

			successIcon = createElement('div', {
				'class': CLASSES.successIcon
			});

			successText = createElement('span', {
				'class': CLASSES.successText
			}, "Congratulations No Issues!");

			successWrapper.appendChild(successIcon);
			successWrapper.appendChild(successText);

			mainWrapper.appendChild(successWrapper);
		}

		mainPanel.appendChild(mainWrapper);

		// Append the main panel to the page.
		document.body.appendChild(mainPanel);

		this.mainPanel = mainPanel;
		this.homeControl = homeControl;
		this.closeControl = mainClose;
		this.statusPanel = statusPanel;
	};

	init() {

		// Check to see if any test failed with warnings and/or errors
		if (this.data.errors > 0 || this.data.warnings > 0) {

			this.resultIndex = {};

			let testCounter = 0;
			let totalIssues = 0;

			// Assign each test result type a counter index
			for (var test in this.data.results) {

				this.data.results[test].testIndex = testCounter;

				this.resultIndex[testCounter] = test;

				totalIssues += this.data.results[test].results.length;

				testCounter++;
			}

			this.totalIssuesCount = totalIssues;

			this.createUI(false);

			//this.populateNextIssue();
		}
		else {

			this.issueExist = false;

			this.createUI(true);
		}
	};

};