function childCount (elem, onlyCount) {

	var childNodes = elem.childNodes;

	if (onlyCount) {

		let count = 0;

		if (typeof onlyCount === "string") {
			onlyCount = [ onlyCount ]; 
		}

		for (var i = 0, len = childNodes.length; i < len; i++) {

			let nodeType = childNodes[i].nodeType;

			if (onlyCount.indexOf(nodeType) !== -1) {

				count++;
			}
		}

		return count;
	}
	else {

		return childNodes.length;
	}
};

// Function to polyfil object.assign or object merge util.
function objectAssign() {

	if (arguments.length > 0) {

		let newObj = {};

		for (var i = 0, len = arguments.length; i < len; i ++) {

			var objArg = arguments[i];

			if (typeof objArg === "object" && !Array.isArray(objArg)) {

				for (var p in objArg) {

			        if (objArg.hasOwnProperty(p)) {

			            try {
			                // Property in destination object set; update its value.
			                if (objArg[p].constructor === Object) {

			                    if (newObj[p].constructor !== Object) {
			                        newObj[p] = {};
			                    }
			                    newObj[p] = this.merge(newObj[p], objArg[p]);

			                }
			                else {
			                    newObj[p] = objArg[p];
			                }

			            }
			            catch (e) {

			                // Property in destination object not set; create it and set its value.
			                newObj[p] = objArg[p];

			            }
			        }

			    }

			    return newObj;

			}
			else {

				continue;
			}

		}

	}

	return false;
};

function elementParents(elem, skipRoot) {

	var domTree = [];

	(function walkDOM(elem, skip) {

		if (!skip) {

			let domElem = {
				elem: elem,
				tagName: elem.nodeName,
				classes: elem.className.split(' '),
				id: elem.getAttribute('id') || false,	
			};

			// Add this element to the dom tree
			domTree.push(domElem);
		}


		let parent = elem.parentNode;

		if (parent) {

			if (parent.nodeName !== "BODY") {

				walkDOM(parent, false);
			}
			
		}


	})(elem, skipRoot);

	return domTree;
};

export {
	childCount,
	objectAssign,
	elementParents
};