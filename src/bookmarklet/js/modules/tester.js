// Test rules
import button from     './rules/button';
import column from     './rules/column';
import duplicates from './rules/duplicates';
import empty from      './rules/empty';
import gridLayout from './rules/gridLayout';
import messages from   './rules/messages';
import table from      './rules/table';
import fields from 	   './rules/fields';

// Load a generic utility script
import * as utilties from "./utilities";

const TEST_RESULT_OBJECT = {
	"name": false,
	"results": false
};

export default class Tester {

	constructor() {

		this.verbose;
		this.testRegistry = [
			button,
			duplicates,
			empty,
			gridLayout,
			column,
			table,
			messages,
			fields
		];

		this.testResults = [];

		this.errors = 0;
		this.warnings = 0;

	};

	// Method to execute all test found in the test rengistry
	testAll() {

		function processReturn(classInstance, resultArray) {

			let errorCount = 0;
			let warningCount = 0;

			// Loop through all of the results and tally up the total errors and warnings!
			for (var r = 0, rLen = resultArray.length; r < rLen; r++) {

				switch(resultArray[r].type) {

					case "error":

						classInstance.errors++;

						errorCount++;

						break;

					case "warning":

						classInstance.warnings++;

						warningCount++;

						break;
				}

			}

			return { 
				"errors": errorCount,
				"warnings": warningCount
			};
		};

		var results = {};

		// Loop through the test registery
		for (var i = 0, len = this.testRegistry.length; i < len; i++) {

			// Create a result Object
			let testResultObj = utilties.objectAssign(TEST_RESULT_OBJECT);

			let testFileName = this.testRegistry[i].name;

			console.log("Executing test:", testFileName);

			// Check to see if we have a multipe part test
			if (Array.isArray(this.testRegistry[i].test)) {

				for (var t = 0, tLen = this.testRegistry[i].test.length; t < tLen; t++) {

					let subTestName = testFileName + " - " + this.testRegistry[i].test[t].name;

					testResultObj.name = subTestName;

					// Execute the test
					testResultObj.results = this.testRegistry[i].test[t].test(this.verbose, utilties);

					if (Array.isArray(testResultObj.results) && testResultObj.results.length) {
						
						var processedCounts = processReturn(this, testResultObj.results);

						testResultObj.errors = processedCounts.errors;
						testResultObj.warnings = processedCounts.warnings;

						// Extend offical results to object.
						results[testResultObj.name] = utilties.objectAssign(testResultObj);
					}
				}

			}
			else if (typeof this.testRegistry[i].test === "function") {

				let testResultObj = utilties.objectAssign(TEST_RESULT_OBJECT);

				// Execute the single level test
				let testResult = this.testRegistry[i].test(this.verbose, utilties);

				if (testResult !== false) {
					results = results.concat(testResult);
				}
			}
		}

		let finalResults = {
			"errors": this.errors,
			"warnings": this.warnings,
			"tests": Object.keys(results).length,
			"results": utilties.objectAssign(results)
		};

		if (finalResults.tests > 0) {

			return finalResults;
		}
		else {

			return false;
		}
	};

}