import Tester from './modules/tester';
import Panel from './modules/panel';

// Load a generic utility script
import { objectAssign } from './modules/utilities';

(function() {

	// Extend the test page class which is the heart of the bookmarklets purpose
	class EmpireBookmarklet extends Tester {

		constructor() {

			super();

			// Variables
			this.exists = {
				fwData: false,
				standardBody: false
			};

			// End test results
			this.testResults = false;

			this.isMockup = null;
			this.isJSP = null;
			this.isTestPage = null;

			this.data = {
				fwData: false
			};

			this.panel = false;

			// Call setup routine
			this.setup();

			// Return a full class object to the browser window.empireBookmarklet property for console access.
			return this;
		}

		// Function will init the bookmarklet
		setup() {

			console.log("Empire 2 Bookmarklet is now loaded!");

			var sectionCollapsed = document.querySelectorAll('section.emp-collapse');

			for (let s = 0, sLen = sectionCollapsed.length; s < sLen; s++) {
				sectionCollapsed[s].classList.remove('emp-collapse');
			}

			// Check for a copy of fwData and make a copy if its found
			if (window.fwData && typeof window.fwData === "object") {

				this.exists.fwData = true;
				this.data.fwData = objectAssign(window.fwData);
			}

			// Select and check for the standard body import div.
			var rootBody = document.getElementById('body-wrapper');

			if (rootBody) {

				this.exists.standardBody = true;

				// Execute all tests
				this.testResults = this.test();

				// Create the display panel;
				this.panel = new Panel(this.testResults);
			}
			else {

				console.error("Empire 2 bookmarklet did not detect this page as an Empire 2 page.");
			}

		};

		// Method to enforce hiding of the panel
		hide() {

			this.panel.hidePanel(this.panel);
		};

		// Method to enforce hiding of the panel
		show() {

			this.panel.showPanel(this.panel);
		};

		test(target) {

			switch (target) {

				default:

					return this.testAll();
					break;
			}
		};

	}

	window.empireBookmarklet = new EmpireBookmarklet();

})();