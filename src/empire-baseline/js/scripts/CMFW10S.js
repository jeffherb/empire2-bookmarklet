define([], function() {

    // Prerender Hook to allow the table to have additional column limits
    var preRenderHook = function(data) {

        console.log("Render hook called");

        if (data.template === "table") {

            console.log(data);

            data.limit = 100;
        }

        return data;
    };

    return {
        preRenderHook: preRenderHook
    };
});
