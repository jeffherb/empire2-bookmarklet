/*! Empire 2
 *  @description  Empire
 *  @version      0.1.0.REL20170428
 *  @copyright    2017 New York State Office of Information Technology Services
 */

define(['jquery', 'cui', 'htmlToDataStore', 'dataStore', 'render'], function ($, cui, htmlToDataStore, ds, render) {

    // Public methods

    // Special arggregate function for creating dataStores.
    var createElementDataStore = function _createElementDataStore (targets, safe, cb) {
        var results;

        if (typeof safe === 'function') {
            cb = safe;
            safe = false;
        }

        if (Array.isArray(targets)) {
            var contents = [];

            for (var i = 0, len = targets.length; i < len; i++) {
                var target = targets[i];
                var targetObject = htmlToDataStore.parseElement(target);

                contents.push(targetObject);
            }

            results = contents;
        }
        else {
            results = htmlToDataStore.parseElement(targets, safe);
        }

        // Call the callback if its defined
        if (typeof cb === 'function') {
            cb(results);
        }
        else {
            return results;
        }
    };

    var fixChangedSpecs = function _fixChangedSpecs () {
        var $tables = $('table');

        if ($tables.length !== 0) {
            $headers = $tables.find('th[data-sort]:not(.emp-sortable)');

            if ($headers.length !== 0) {
                $headers.addClass('emp-sortable');
            }
        }
    };

    var createPageDataStore = function _createPageDataStore (types, cb) {
        // Stub out fwData globals with usable placeholder data

        if (!window.fwData) {
            window.fwData = {};
        }

        // Menu
        if (!window.fwData.menu) {
            window.fwData.menu = {};
        }

        if (!window.fwData.menu.display) {
            window.fwData.menu.display = {
                className: 'emp',
            };
        }
        if (!window.fwData.menu.items) {
            window.fwData.menu.items = [
                {label: 'Client Information',items: [{label: 'Inquire On Client',url:'#', bookmarkable: true, resourceId:'5099ce92'},{label: 'Maintain Client', items: [{label: 'Item 1', items: [{label: 'Item 1',id: 'Item1',url:'#', bookmarkable: true, resourceId:'c81b200a'},{label: 'Item 2',id: 'Item2',url:'#', bookmarkable: true, resourceId:'a8d7b4b9'}]},{label: 'Item 2', items: [{label: 'Item 2.1',id: 'Item21',url:'#', bookmarkable: true, resourceId:'71861028'},{label: 'Item 2.2', items: [{label: 'Item 2.2.1',id: 'Item221',url:'#', bookmarkable: true, resourceId:'b11d6c69'}]},{label: 'Item 2.3',id: 'item23',url:'#', bookmarkable: true, resourceId:'e9834a29'}]},{label: 'Item 3',id: 'item3',url:'#', bookmarkable: true, resourceId:'39ea4c7d'}]}, {label: 'Search for Client',id: 'SearchforClient',tooltip: 'Looking for a client? Try here.',url: 'ht bookmarkable: true, tp://tax.ny.gov',urlTarget: '_blank'}, {label: 'Event Management',id: 'EventManagement',url:'#', bookmarkable: true, resourceId:'14750a72'}, {label: 'Business Profile Inquiry',id: 'BusinessProfileInquiry',url:'#', bookmarkable: true, resourceId:'e13371f8'}, {label: 'Address Inq',id: 'AddressInq',url:'#', bookmarkable: true, resourceId:'a95f8180'}]},
                {label: 'Account Information',items: [{label: 'Inquire on Account',items: [{label: 'Converstion Inq',id: 'ConverstionInq',url:'#', bookmarkable: true, resourceId:'e7dcc941'},{label: 'Accounting Inq',items: [{label: 'Item 1',id: 'Item1a',url:'#', bookmarkable: true, resourceId:'197a5c5e'},{label: 'Item 2',id: 'Item2a',url:'#', bookmarkable: true, resourceId:'475399da'}]},{label: 'Taxpayer Account Inq',id: 'TaxpayerAccountInq',url:'#', bookmarkable: true, resourceId:'725bbb90'},{label: 'Reqturns Inq',id: 'ReqturnsInq',url:'#', bookmarkable: true, resourceId:'b5a51cca'},{label: 'Audit Screening',items: [{label: 'Audit Screening 1',id: 'AuditScreening1',url:'#', bookmarkable: false, resourceId:'dd4356f9'},{label: 'Audit Screening 2',id: 'AuditScreening2',url:'#', bookmarkable: true, resourceId:'2fad2026'}]}]}, {label: 'Search on Account',id: 'SearchonAccount',url:'#', bookmarkable: true, resourceId:'1a580afd'}, {label: 'Collection Agency',id: 'CollectionAgency',url:'#', bookmarkable: true, resourceId:'abdabc45'}]},
                {label: 'Work Management',items: [{label: 'Worklist Management',id: 'WorklistManagement',url:'#', bookmarkable: true, resourceId:'eb6ab635'}, {label: 'Work Item Search',id: 'WorkItemSearch',url:'#', bookmarkable: true, resourceId:'e137df4a'}, {label: 'Audit',items: [{label: 'Audit Case Work',id: 'AuditCaseWork',url:'#', bookmarkable: true, resourceId:'e12677f6'},{label: 'Inventory Management',id: 'InventoryManagement',url:'#', bookmarkable: true, resourceId:'a0b3a228'}]}, {label: 'BCMS',items: [{label: 'BCMS Casework',id: 'BCMSCasework',url:'#', bookmarkable: true, resourceId:'590dfa9d'},{label: 'Inventory Management',id: 'InventoryManagement',url:'#', bookmarkable: true, resourceId:'5190086e'}]}]},
                {label: 'Special Services',items: [{label: 'Reporting',id: 'Reporting',url:'#', bookmarkable: true, resourceId:'ed19f4b3'}, {label: 'EDMS',id: 'EDMS',url:'#', bookmarkable: true, resourceId:'9883fa4c'}, {label: 'You should never see this and its label length should have no impact on the other items in this group',dividerType: 'blank'}, {label: 'Order Forms',id: 'OrderForms',url:'#', bookmarkable: true, resourceId:'69ba4820'}]}
            ];
        }

        // Favorites

        if (!window.fwData.favorites) {
            window.fwData.favorites = {};
        }

        if (!window.fwData.favorites.data) {
            window.fwData.favorites.data = {
                id: 'favorites',
            };
        }

        if (!window.fwData.favorites.data.tabsets) {
            window.fwData.favorites.data.tabsets = [];
        }

        if (!window.fwData.favorites.data.tabsets.length) {
            window.fwData.favorites.data.tabsets = [
                {
                    id: 'LP123',
                    url: 'LP123.gateway?jadeAction=TB0822N_BUSINESS_SUMMARY_RETRIEVE_ACTION&amp;fwFromNav=Y',
                    name: 'Liability Period',
                    label: 'Liability Period',
                    parent: 'favorites',
                },
                {
                    id: 'TI_folder',
                    label: 'TI',
                    isOpen: true,
                    parent: 'favorites',
                    tabsets: [
                        {
                            id: 'CPERSI01',
                            url: 'CPERSI01.gateway?jadeAction=TB0822N_BUSINESS_SUMMARY_RETRIEVE_ACTION&amp;fwFromNav=Y',
                            name: 'Individual Taxpayer Profile Inquiry',
                            label: 'Ind TP Inquiry',
                            parent: 'favorites',
                        },
                    ],
                },
                {
                    id: 'CM123',
                    url: 'CM123.gateway',
                    name: 'Case Management',
                    label: 'Case Management',
                    parent: 'favorites',
                },
            ];
        }

        // Let components know that this is an HTML-based mockup and is not running on a web server
        window.fwData.isMockup = true;

        // Move cb over to the right variable just incase no types are defined
        if (typeof types === 'function') {
            cb = types;
            types = undefined;
        }

        // Check to see something is defined, otherwise here are our defaults.
        if (!types) {
            types = ['table', 'section.emp-tabs'];
        }

        // Array of elements we want to process.
        var elements = types;

        var result = htmlToDataStore.parsePage(elements);

        // Call our generic fix function that is being used to polyfill changes happening over time.
        fixChangedSpecs();

        // Find tables
        var $tables = $('table');
        var $tabs = $('section.emp-tabs');

        if ($tables.length) {

            $tables.each(function () {
                var $table = $(this);
                var id = $table.attr('id');
                var store = ds.getStore($table.attr('data-store-id'));

                // Get the parent
                var parent = $table.parent();

                if (id === 'empty-table') {
                    delete store.body;
                }

                var root = $table.parent('.emp-table').eq(0);

                if (store && typeof store === "object") {
                    // Rerender the table per the new render code and optimization spec (JH: 3/21/2016)
                    render.section(undefined, store, 'return', function(html, store) {

                        var currentTable = root[0];
                        var parentContainer = currentTable.parentNode;

                        parentContainer.insertBefore(html.firstChild, currentTable);

                        parentContainer.removeChild(currentTable);

                    });
                }

            });
        }

        if ($tabs.length) {

            $tabs.each(function() {

                var $tabSection = $(this);
                var id = $tabSection.attr('id');

                if (!id) {

                    $tabSection.attr('id', 'UIHTMLDATASTORETEMPTABS');
                }

                var store =  ds.getStore($tabSection.attr('data-store-id'));

                var parent = $tabSection.parent();

                if (store && typeof store === "object") {

                    // Rerender the table per the new render code and optimization spec (JH: 3/21/2016)
                    render.section(undefined, store, 'return', function(html, store) {

                        var currentTabSection = $tabSection[0];
                        var parentContainer = currentTabSection.parentNode;

                        parentContainer.insertBefore(html.firstChild, currentTabSection);

                        parentContainer.removeChild(currentTabSection);

                    });

                }

            });

        }

        if (typeof cb === 'function') {
            cb(result);
        }
        else {
            return result;
        }
    };

    // Public methods of the `uimockup` object
    return {
        createPageDataStore: createPageDataStore,
        createElementDataStore: createElementDataStore
    };
});
