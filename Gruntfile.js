var babel = require('rollup-plugin-babel');

module.exports = function(grunt) {

    grunt.initConfig({

    	bake: {
    		dev: {
    			options: {
    				content: {
    					domain: "http://localhost:9999",
    					path: 'bookmarklet/js/bookmarklet.js',
                        csspath: 'bookmarklet/css/bookmarklet.css'
    				}
    			},
    			files: {
    				'dist/installPage/index.html': 'src/installPage/index.html'
    			}
    		}
    	},

    	clean: {
            all: {
                src: ['dist'],
            },
            bookmarklet: {
                src: ['dist/bookmarklet', 'dist/installPage', 'dist/tests']
            }
        },

    	copy: {
    		"testpages": {
    			files: [
                    {
                        expand: true,
                        cwd: 'src/tests',
                        src: ['**'],
                        dest: 'dist/tests',
                        filter: 'isFile',
                    },
    			]
    		},
            "installImages": {
                "files": [
                    {
                        expand: true,
                        cwd: 'src/installPage/images',
                        src: ['**'],
                        dest: 'dist/installPage/',
                        filter: 'isFile',
                    }
                ]
            },
            "empire-baseline": {
                files: [
                    {
                        expand: true,
                        cwd: 'src/empire-baseline',
                        src: ['**'],
                        dest: 'dist/empire-baseline',
                        filter: 'isFile',
                    },
                ]
            }
    	},

    	connect: {
    		server: {
    			options: {
    				port: 9999,
    				hostname: '*',
    				base: 'dist'
    			}
    		}
    	},

		sass: {
			options: {
				sourceMap: true
			},
			dist: {
				files: {
					'dist/installPage/main.css': 'src/installPage/scss/main.scss',
                    'dist/bookmarklet/css/bookmarklet.css': 'src/bookmarklet/scss/bookmarklet.scss'
				}
			}
		},

    	rollup: {
    		"options": {
    			plugins: [
    				babel({
    					babelrc: false,
    					plugins: ["external-helpers"],
    					presets: [
    						[
    							"env",
    							{
    								target: {
    									"browsers": ["latest 2 versions", "ie 11"] 
    								},
    								"modules": false
    							}
    						]
    					],
						exclude: [ 'node_modules/**' ]
    				})
    			]
    		},
    		"files": {
    			'dest': 'dist/bookmarklet/js/bookmarklet.js',
    			'src': 'src/bookmarklet/js/index.js'
    		}
    	},

    	watch: {
            options: {
                //livereload: true,
                interrupt: true,
                spawn: false
            },
            project: {
				files: 'src/**/*.*',
				tasks: [
		        	'clean:bookmarklet',
		        	'bake:dev',
                    'copy:testpages',
                    'copy:installImages',
		        	'sass',
		        	'rollup',
				]
			}
    	}

    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('dev', 'Development', function (args) {

        grunt.task.run([
        	'clean:all',
        	'bake:dev',
            'copy',
        	'sass',
        	'rollup',
        	'connect',
        	'watch'
        ]);

    });

    grunt.registerTask('prod', 'Production', function (args) {

        // ===========
        // == BAKE ===
        // ===========
        var bake = grunt.config.get('bake');

        bake.dev.options.content.domain = "http://dvm.int.nystax.gov"
        bake.dev.options.content.path = "emp/bookmarklet/js/bookmarklet.js";
        bake.dev.options.content.csspath = "emp/bookmarklet/css/bookmarklet.css";

        grunt.config.set('bake', bake);

        grunt.task.run([
            'clean:all',
            'bake:dev',
            'copy',
            'sass',
            'rollup'
        ]);

    });

    // Set the default task to the development build
    grunt.registerTask('default', 'dev');
};